####################
#Dragon Coast
####################
#Portroy Merchant's Guild (Portnamm)
portnamm_portroy_merchants_guild = {
	start = 126
	date = 01.01.01
	time = {
		months = 0
	}
	build_cost = 0
	can_be_moved = no
	move_days_per_unit_distance = 1
	starting_tier = 1
	type = monument
	build_trigger = {	
	}
	on_built = {		
	}
	on_destroyed = {	
	}
	can_use_modifiers_trigger = {
		AND = {
			province_is_or_accepts_culture = yes
			OR = {
				culture_group = gnomish
				culture_group = kobold
				culture = castanorian
			}
		}
	}	
	can_upgrade_trigger = {
		AND = {
			province_is_or_accepts_culture = yes
			OR = {
				culture_group = gnomish
				culture_group = kobold
				culture = castanorian
			}
		}
	}	
	keep_trigger = {
	}
	tier_0 = {
		upgrade_time = {
			months = 0
		}
		cost_to_upgrade = {
			factor = 0
		}
		province_modifiers = {
		}
		area_modifier = {
		}
		country_modifiers = {	
		}
		on_upgraded = {
		}
	}
	tier_1 = {
		upgrade_time = {
			months = 120
		}
		cost_to_upgrade = {
			factor = 1000
		}
		province_modifiers = {
			local_production_efficiency = 0.1
		}
		area_modifier = {
		}
		country_modifiers = {
			trade_efficiency = 0.025
			ship_power_propagation = 0.05
		}
		on_upgraded = {
		}
	}
	tier_2 = {
		upgrade_time = {
			months = 240
		}
		cost_to_upgrade = {
			factor = 2500
		}
		province_modifiers = {
			local_production_efficiency = 0.15
			province_trade_power_value = 5
		}
		area_modifier = {
		}
		country_modifiers = {
			trade_efficiency = 0.05
			ship_power_propagation = 0.1
			burghers_loyalty_modifier = 0.025
		}
		on_upgraded = {
			define_advisor = {
			type = trader
			name = "Jarnolt Iondo"
			skill = 3
			culture = creek_gnome
			religion = regent_court
			location = 126
			discount = yes
			}
		}
	}
	tier_3 = {
		upgrade_time = {
			months = 480
		}
		cost_to_upgrade = {
			factor = 5000
		}
		province_modifiers = {
			local_production_efficiency = 0.25
			province_trade_power_value = 10
		}
		area_modifier = {
		}
		country_modifiers = {
			trade_efficiency = 0.1
			ship_power_propagation = 0.15
			burghers_loyalty_modifier = 0.05
			merchants = 1
		}
		on_upgraded = {
		}
	}
}
#Kobildzex Guild of Trapsmiths (Bluescale)
kobildzan_kobildzex_guild_of_trapsmiths = {
	start = 189
	date = 01.01.01
	time = {
		months = 0
	}
	build_cost = 0
	can_be_moved = no
	move_days_per_unit_distance = 1
	starting_tier = 0
	type = monument
	build_trigger = {	
	}
	on_built = {		
	}
	on_destroyed = {	
	}
	can_use_modifiers_trigger = {
		AND = {
			province_is_or_accepts_culture = yes
			OR = {
				culture_group = kobold
				culture = castanorian
			}
		}
	}	
	can_upgrade_trigger = {
		AND = {
			province_is_or_accepts_culture = yes
			OR = {
				culture_group = kobold
				culture = castanorian
			}
		}
	}	
	keep_trigger = {
	}
	tier_0 = {
		upgrade_time = {
			months = 0
		}
		cost_to_upgrade = {
			factor = 0
		}
		province_modifiers = {
		}
		area_modifier = {
		}
		country_modifiers = {	
		}
		on_upgraded = {
		}
	}
	tier_1 = {
		upgrade_time = {
			months = 120
		}
		cost_to_upgrade = {
			factor = 1000
		}
		province_modifiers = {
			local_development_cost = -0.05
			local_production_efficiency = 0.1
			local_defensiveness = 0.1
		}
		area_modifier = {
		}
		country_modifiers = {
			defensiveness = 0.05
			enemy_core_creation = 0.1
		}
		on_upgraded = {
		}
	}
	tier_2 = {
		upgrade_time = {
			months = 240
		}
		cost_to_upgrade = {
			factor = 2500
		}
		province_modifiers = {
			local_development_cost = -0.1
			local_production_efficiency = 0.15
			local_defensiveness = 0.2
		}
		area_modifier = {
		}
		country_modifiers = {
			hostile_attrition = 0.5
			defensiveness = 0.1
			enemy_core_creation = 0.2
		}
		on_upgraded = {
		}
	}
	tier_3 = {
		upgrade_time = {
			months = 480
		}
		cost_to_upgrade = {
			factor = 5000
		}
		province_modifiers = {
			local_development_cost = -0.15
			local_production_efficiency = 0.2
			local_defensiveness = 0.33
		}
		area_modifier = {
		}
		country_modifiers = {
			hostile_attrition = 1
			defensiveness = 0.15
			enemy_core_creation = 0.33
		}
		on_upgraded = {
			owner = {
				mission_reward_artifice_points_3 = yes
			}
		}
	}
}
#The Dragonhoard (Bluescale)
the_dragonhoard = {
	start = 189
	date = 01.01.01
	time = {
		months = 0
	}
	build_cost = 0
	can_be_moved = no
	move_days_per_unit_distance = 1
	starting_tier = 0
	type = monument
	build_trigger = {	
	}
	on_built = {		
	}
	on_destroyed = {	
	}
	can_use_modifiers_trigger = {
		AND = {
			has_owner_religion = yes
			religion_group = dragon_cult
			province_is_or_accepts_culture = yes
			OR = {
				culture_group = kobold
				culture = castanorian
			}
		}
	}	
	can_upgrade_trigger = {
		AND = {
			has_owner_religion = yes
			religion_group = dragon_cult
			province_is_or_accepts_culture = yes
			OR = {
				culture_group = kobold
				culture = castanorian
			}
		}
	}	
	keep_trigger = {
	}
	tier_0 = {
		upgrade_time = {
			months = 0
		}
		cost_to_upgrade = {
			factor = 0
		}
		province_modifiers = {
		}
		area_modifier = {
		}
		country_modifiers = {	
		}
		on_upgraded = {
		}
	}
	tier_1 = {
		upgrade_time = {
			months = 120
		}
		cost_to_upgrade = {
			factor = 1000
		}
		province_modifiers = {
			local_development_cost = -0.05
			trade_goods_size_modifier = 0.05
		}
		area_modifier = {
		}
		country_modifiers = {
			loot_amount = 0.25
			tolerance_own = 1
		}
		on_upgraded = {
		}
	}
	tier_2 = {
		upgrade_time = {
			months = 240
		}
		cost_to_upgrade = {
			factor = 2500
		}
		province_modifiers = {
			local_development_cost = -0.1
			trade_goods_size_modifier = 0.1
		}
		area_modifier = {
		}
		country_modifiers = {
			loot_amount = 0.5
			tolerance_own = 1.5
			land_morale = 0.025
		}
		on_upgraded = {
		}
	}
	tier_3 = {
		upgrade_time = {
			months = 480
		}
		cost_to_upgrade = {
			factor = 5000
		}
		province_modifiers = {
			local_development_cost = -0.15
			trade_goods_size_modifier = 0.15
		}
		area_modifier = {
		}
		country_modifiers = {
			loot_amount = 1
			tolerance_own = 2
			land_morale = 0.05
		}
		on_upgraded = {
		}
	}
}