####################
#Alenic Reach
####################
#Bayvic, Grandest City of the Reach
bayvic_grandest_city_of_the_reach = {
	start = 723
	date = 01.01.01
	time = {
		months = 0
	}
	build_cost = 0
	can_be_moved = no
	move_days_per_unit_distance = 1
	starting_tier = 1
	type = monument
	build_trigger = {	
	}
	on_built = {		
	}
	on_destroyed = {	
	}
	can_use_modifiers_trigger = {
		AND = {
			province_is_or_accepts_culture = yes
			OR = {
				culture_group = alenic
				culture = city_goblin
				culture = white_reachman
				culture = castanorian
			}
		}
	}	
	can_upgrade_trigger = {
		AND = {
			province_is_or_accepts_culture = yes
			OR = {
				culture_group = alenic
				culture = city_goblin
				culture = white_reachman
				culture = castanorian
			}
		}
	}	
	keep_trigger = {
	}
	tier_0 = {
		upgrade_time = {
			months = 0
		}
		cost_to_upgrade = {
			factor = 0
		}
		province_modifiers = {
		}
		area_modifier = {
		}
		country_modifiers = {	
		}
		on_upgraded = {
		}
	}
	tier_1 = {
		upgrade_time = {
			months = 120
		}
		cost_to_upgrade = {
			factor = 1000
		}
		province_modifiers = {
			trade_goods_size_modifier = 0.1
		}
		area_modifier = {
		}
		country_modifiers = {
			global_sailors_modifier = 0.05
			galley_power = 0.025
		}
		on_upgraded = {
		}
	}
	tier_2 = {
		upgrade_time = {
			months = 240
		}
		cost_to_upgrade = {
			factor = 2500
		}
		province_modifiers = {
			trade_goods_size_modifier = 0.15
		}
		area_modifier = {
		}
		country_modifiers = {
			global_sailors_modifier = 0.1
			galley_power = 0.05
		}
		on_upgraded = {
			add_base_production = 2
		}
	}
	tier_3 = {
		upgrade_time = {
			months = 480
		}
		cost_to_upgrade = {
			factor = 5000
		}
		province_modifiers = {
			trade_goods_size_modifier = 0.25
		}
		area_modifier = {
		}
		country_modifiers = {
			global_sailors_modifier = 0.15
			galley_power = 0.1
			siege_blockade_progress = 1
		}
		on_upgraded = {
			add_base_production = 2
			owner = {
				add_country_modifier = {
					name = galley_veterans
					duration = 10950
				}
			}
		}
	}
}
#Icewind Palace of Commerce (Celmaldor)
celmaldor_icewind_palace_of_commerce = {
	start = 696
	date = 01.01.01
	time = {
		months = 0
	}
	build_cost = 0
	can_be_moved = no
	move_days_per_unit_distance = 1
	starting_tier = 1
	type = monument
	build_trigger = {	
	}
	on_built = {		
	}
	on_destroyed = {	
	}
	can_use_modifiers_trigger = {
		AND = {
			province_is_or_accepts_culture = yes
			OR = {
				culture_group = alenic
				culture = moon_elf
				culture = white_reachman
				culture = castanorian
			}
		}
	}	
	can_upgrade_trigger = {
		AND = {
			province_is_or_accepts_culture = yes
			OR = {
				culture_group = alenic
				culture = moon_elf
				culture = white_reachman
				culture = castanorian
			}
		}
	}	
	keep_trigger = {
	}
	tier_0 = {
		upgrade_time = {
			months = 0
		}
		cost_to_upgrade = {
			factor = 0
		}
		province_modifiers = {
		}
		area_modifier = {
		}
		country_modifiers = {	
		}
		on_upgraded = {
		}
	}
	tier_1 = {
		upgrade_time = {
			months = 120
		}
		cost_to_upgrade = {
			factor = 1000
		}
		province_modifiers = {
			trade_value_modifier = 0.05
		}
		area_modifier = {
		}
		country_modifiers = {
			light_ship_power = 0.025
			global_ship_trade_power = 0.1
		}
		on_upgraded = {
		}
	}
	tier_2 = {
		upgrade_time = {
			months = 240
		}
		cost_to_upgrade = {
			factor = 2500
		}
		province_modifiers = {
			province_trade_power_value = 5
			trade_value_modifier = 0.05
		}
		area_modifier = {
		}
		country_modifiers = {
			light_ship_power = 0.1
			global_ship_trade_power = 0.15
		}
		on_upgraded = {
		}
	}
	tier_3 = {
		upgrade_time = {
			months = 480
		}
		cost_to_upgrade = {
			factor = 5000
		}
		province_modifiers = {
			province_trade_power_value = 10
			trade_value_modifier = 0.15
			allowed_num_of_buildings = 1
		}
		area_modifier = {
		}
		country_modifiers = {
			light_ship_power = 0.1
			global_ship_trade_power = 0.2
			merchants = 1
		}
		on_upgraded = {
			owner = {
				add_country_modifier = {
					name = anbenncost_trade_fleet_modifier
					duration = 10950
				}
			}
			add_unit_construction = {
				type = light_ship
				amount = 15
				speed = 0.25
				cost = 0.1
			}
		}
	}
}