#########################################
#			  Created by				#
#			   Minnator					#
#########################################

namespace = Qol_events

#choose manufactory
country_event = {
	id = Qol_events.1
	title = Qol_events.1.t
	desc = Qol_events.1.d
	picture = GREAT_BUILDING_eventPicture
	
	is_triggered_only = yes
	
	trigger = {  }
	
	mean_time_to_happen = {
		days = 1
	}
	
	#option = {
	#	name = "Qol_option_manufactory"
	#	trigger = {
	#		adm_tech = 5
	#	}
	#}
	
	option = {
		name = "Qol_option_rampart"
		trigger = {
			adm_tech = 5
		}
		country_event = { id = Qol_events.2 }	#ramparts
	}
	
	option = {
		name = "Qol_option_impressnment_offices"
		trigger = {
			adm_tech = 7
		}
		country_event = { id = Qol_events.3 }	#Impressnment Offices
	}
	
	option = {
		name = "Qol_option_state_house"
		trigger = {
			adm_tech = 12
		}
		country_event = { id = Qol_events.4 }	#State House
	}
	
	option = {
		name = "Qol_option_soliders_household"
		trigger = {
			adm_tech = 15
		}
		country_event = { id = Qol_events.5 }	#Solider's Household
	}
	
	option = {
		name = "Qol_option_furnace"
		trigger = {
			adm_tech = 21
		}
		country_event = { id = Qol_events.6 }	#Furnace
	}
		
	option = {
		name = "Qol_option_you_can_not_have_these_yet"
		
	}
}

#ramparts
country_event = {
	id = Qol_events.2
	title = Qol_events.2.t
	desc = Qol_events.2.d
	picture = GREAT_BUILDING_eventPicture
	
	is_triggered_only = yes
	
	trigger = {  }
	
	mean_time_to_happen = {
		days = 1
	}
	
	option = {
		name = "Qol_make_place_for_ramparts_mountains"
		every_owned_province = {
			limit = {
				has_terrain = mountain
				OR = {
					has_building = fort_15th
					has_building = fort_16th
					has_building = fort_17th
					has_building = fort_18th
				}
			}
			remove_building = wharf
			remove_building = weapons
			remove_building = textile
			remove_building = plantations
			remove_building = tradecompany
			remove_building = farm_estate
			remove_building = mills
			#has_building = furnace #furnaces have a higher Priority!
			remove_building = soldier_households
			remove_building = impressment_offices
			remove_building = state_house
		}
	}
	
	option = {
		name = "Qol_make_place_for_ramparts_highlands"
		every_owned_province = {
			limit = {
				has_terrain = highlands
				OR = {
					has_building = fort_15th
					has_building = fort_16th
					has_building = fort_17th
					has_building = fort_18th
				}
			}
			remove_building = wharf
			remove_building = weapons
			remove_building = textile
			remove_building = plantations
			remove_building = tradecompany
			remove_building = farm_estate
			remove_building = mills
			#has_building = furnace #furnaces have a higher Priority!
			remove_building = soldier_households
			remove_building = impressment_offices
			remove_building = state_house
		}
	}
	#yes it would maybe make sense to include further options for different terrains, but this will never be used one way or another
	option = {
		name = "Qol_option_no"		
	}
}

#Impressnment Offices
country_event = {
	id = Qol_events.3
	title = Qol_events.3.t
	desc = Qol_events.3.d
	picture = GREAT_BUILDING_eventPicture
	
	is_triggered_only = yes
	
	trigger = {  }
	
	mean_time_to_happen = {
		days = 1
	}
	
	option = {
		name = "Qol_make_place_for_impressnment_offices"
		every_owned_province = {
			limit = {
				has_port = yes
				OR = {
					trade_goods = salt
					trade_goods = fish
					trade_goods = naval_supplies
					trade_goods = tropical_wood
				}
			}
			remove_building = wharf
			remove_building = weapons
			remove_building = textile
			remove_building = plantations
			remove_building = tradecompany
			remove_building = farm_estate
			remove_building = mills
			#has_building = furnace #furnaces have a higher Priority!
			remove_building = soldier_households
			remove_building = ramparts
			remove_building = state_house
		}
	}
	
	option = {
		name = "Qol_option_no"		
	}
}

#State House
country_event = {
	id = Qol_events.4
	title = Qol_events.4.t
	desc = Qol_events.4.d
	picture = GREAT_BUILDING_eventPicture
	
	is_triggered_only = yes
	
	trigger = {  }
	
	mean_time_to_happen = {
		days = 1
	}
	
	option = {
		name = "Qol_make_place_for_state_house"
		every_owned_province = {
			limit = {
				OR = {
					trade_goods = paper
					trade_goods = glass
					trade_goods = gems
				}
			}
			remove_building = wharf
			remove_building = weapons
			remove_building = textile
			remove_building = plantations
			remove_building = tradecompany
			remove_building = farm_estate
			remove_building = mills
			#has_building = furnace #furnaces have a higher Priority!
			remove_building = soldier_households
			remove_building = ramparts
			remove_building = impressment_offices
		}
	}
	
	option = {
		name = "Qol_option_no"		
	}
}

#Solider's Household
country_event = {
	id = Qol_events.5
	title = Qol_events.5.t
	desc = Qol_events.5.d
	picture = GREAT_BUILDING_eventPicture
	
	is_triggered_only = yes
	
	trigger = {  }
	
	mean_time_to_happen = {
		days = 1
	}
	
	option = {
		name = "Qol_make_place_for_soliders_household"
		every_owned_province = {
			limit = {
				OR = {
					trade_goods = grain
					trade_goods = fish
					trade_goods = livestock
					trade_goods = wine
					trade_goods = fungi
					trade_goods = serpentbloom
				}
			}
			remove_building = wharf
			remove_building = weapons
			remove_building = textile
			remove_building = plantations
			remove_building = tradecompany
			remove_building = farm_estate
			remove_building = mills
			#has_building = furnace #furnaces have a higher Priority!
			remove_building = state_house
			remove_building = ramparts
			remove_building = impressment_offices
		}
	}
	
	option = {
		name = "Qol_option_no"		
	}
}

#Furnace
country_event = {
	id = Qol_events.6
	title = Qol_events.6.t
	desc = Qol_events.6.d
	picture = GREAT_BUILDING_eventPicture
	
	is_triggered_only = yes
	
	trigger = {  }
	
	mean_time_to_happen = {
		days = 1
	}
	
	option = {
		name = "Qol_make_place_for_furnaces"
		every_owned_province = {
			limit = {
				trade_goods = coal
			}
			remove_building = wharf
			remove_building = weapons
			remove_building = textile
			remove_building = plantations
			remove_building = tradecompany
			remove_building = farm_estate
			remove_building = mills
			remove_building = soldier_households
			remove_building = state_house
			remove_building = ramparts
			remove_building = impressment_offices
		}
	}
	
	option = {
		name = "Qol_option_no"		
	}
}
